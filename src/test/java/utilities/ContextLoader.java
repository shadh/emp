package utilities;

import models.Config;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;

public class ContextLoader {

    protected static JSONObject obj;

    public static ContextLoader contextData = new ContextLoader();

    private ContextLoader(){

    }

    public static ContextLoader parseConfig(File file){
        obj = JsonParser.convertJsonToObject(file);
        return contextData;
    }

    public static String getContext(String contextName){

        return obj.get(contextName).toString();
    }

    public  static JSONArray getArrayData(String configName){
        return (JSONArray) obj.get(configName);
    }

}

package utilities;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lib.HttpClient;
import models.Config;
import models.Header;
import models.HttpResponse;
import org.json.simple.JSONObject;

import java.util.List;

public class WrapperRestAssured implements HttpClient {

    private String base_url;
    private Config config;
    private JSONObject requestBody;

    public WrapperRestAssured(Config config){
        this.base_url = config.getConnectionType() + "//"+ config.getHosts() + ":" + config.getPort();
        this.config = config;
    }

    @Override
    public HttpResponse get(String apiContext){
        RestAssured.baseURI = this.base_url;
        RequestSpecification httpRequest = RestAssured.given();
        for(Header header : this.config.getHeaders()){
            httpRequest.header(header.getName(), header.getValue());
        }
        Response response = httpRequest.request(Method.GET, apiContext);
        return new HttpResponse().setMessage(response.getBody().print()).setStatusCode(response.getStatusCode());
    }

    @Override
    public HttpClient requestBody(JSONObject requestBody){
        /*
        JSONObject updateData = new JSONObject();
        updateData.put("name", "Aarna");
        */
        this.requestBody = requestBody;
        return this;
    }

    @Override
    public HttpResponse post(String apiContext) {
        RestAssured.baseURI = this.base_url;

        RequestSpecification httpRequest = RestAssured.given();


        for(Header header : this.config.getHeaders()){
            httpRequest.header(header.getName(), header.getValue());
        }

        httpRequest.body(this.requestBody.toJSONString());
        Response response = httpRequest.request(Method.POST, apiContext);

        /*statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);

        JsonPath newData = response.jsonPath();
        String name = newData.get("name");
        Assert.assertEquals(name, "Aarna");

        */
        return new HttpResponse().setMessage(response.getBody().print()).setStatusCode(response.getStatusCode());
    }

    @Override
    public HttpResponse delete(String apiContext) {
        RestAssured.baseURI = "https://reqres.in/api/users/";

        RequestSpecification httpRequest = RestAssured.given();

        Response response = httpRequest.request(Method.DELETE, apiContext);
        return new HttpResponse().setStatusCode(response.getStatusCode()).setMessage(response.getBody().print());
    }

    @Override
    public HttpResponse put(String apiContext) {
        RestAssured.baseURI = this.base_url;

        RequestSpecification httpRequest = RestAssured.given();


        for(Header header : this.config.getHeaders()){
            httpRequest.header(header.getName(), header.getValue());
        }

        httpRequest.body(this.requestBody.toJSONString());
        Response response = httpRequest.request(Method.PUT, apiContext);

        return new HttpResponse().setMessage(response.getBody().print()).setStatusCode(response.getStatusCode());
    }

    @Override
    public String getBaseUrl() {
        return this.base_url;
    }

    @Override
    public HttpClient addHeader(Header header){
        this.config.addHeader(header);
        return this;
    }

    @Override
    public HttpClient removeAllHeader(){
        List<Header> headers = this.config.getHeaders();
        for(Header header : headers) {
            this.config.removeHeader(header.getName());
        }
        return this;
    }
}

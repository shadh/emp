package utilities;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.util.Scanner;

public class JsonParser {

    public static JSONObject convertJsonToObject(String data) {
        JSONObject obj = null;
        try {
            JSONParser parser = new JSONParser();
            obj = (JSONObject) parser.parse(data);
        }
        catch (ParseException e){
            System.err.println("Json string parsing error!!!");
            e.printStackTrace();
        }
        return obj;
    }

    public static JSONObject convertJsonToObject(File file){
        String data = null;
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                if(data == null){
                    data = "";
                }
                data = data + scanner.nextLine();

            }
            scanner.close();
        } catch (Exception e) {
            System.out.println("Error parsing json file");
            e.printStackTrace();
        }
        //System.out.println(data);
        return convertJsonToObject(data);
    }

}

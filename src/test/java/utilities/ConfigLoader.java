package utilities;

import models.Config;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;

public class ConfigLoader {

    protected static JSONObject obj;

    public static ConfigLoader configData = new ConfigLoader();

    private ConfigLoader(){

    }

    public static ConfigLoader parseConfig(File file){
        obj = JsonParser.convertJsonToObject(file);
        return configData;
    }

    public static String getData(String configName){

        return obj.get(configName).toString();
    }

    public  static JSONArray getArrayData(String configName){
        return (JSONArray) obj.get(configName);
    }

    public Config getConfig(){

        Config config = new Config();

        return config;
    }

}


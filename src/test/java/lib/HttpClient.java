package lib;

import models.Header;
import models.HttpResponse;
import org.json.simple.JSONObject;

public interface HttpClient {

    public HttpResponse get(String apiContext);
    public HttpResponse post(String apiContext);
    public HttpResponse delete(String apiContext);
    public HttpResponse put(String apiContext);
    public String getBaseUrl();
    public HttpClient addHeader(Header header);
    public HttpClient removeAllHeader();
    public HttpClient requestBody(JSONObject requestBody);
}

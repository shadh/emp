package lib;

import models.Employee;
import org.json.simple.JSONObject;
import utilities.JsonParser;

import java.util.HashMap;
import java.util.Map;

public class EmployeeBuilder {

    public Employee employee;

    public EmployeeBuilder withName(String name){
        this.employee.setEmployee_name(name);
        return this;
    }

    public EmployeeBuilder withAge(int age){
        this.employee.setAge(age);
        return this;
    }

    public EmployeeBuilder withSalary(String salary) {
        this.employee.setEmployee_salary(salary);
        return this;
    }

    public EmployeeBuilder withProfileImage(String profileImage){
        this.employee.setProfile_image(profileImage);
        return this;
    }

    public Employee getEmployee(){
        return this.employee;
    }

    public JSONObject getEmployeeJSON(){
        JSONObject object = new JSONObject();

        Map<String, String> map = new HashMap<String, String>();

        if(this.employee.getAge() != null){
            map.put("age", String.valueOf(this.employee.getAge()));
        }
        if(this.employee.getEmployee_name() != null){
            map.put("employee_name", this.employee.getEmployee_name());
        }
        if(this.employee.getEmployee_salary() != null){
            map.put("employee_salary",this.employee.getEmployee_salary());
        }
        if(this.employee.getId() != null){
            map.put("id", String.valueOf(this.employee.getId()));
        }
        if(this.employee.getProfile_image() != null){
            map.put("profile_image", this.employee.getProfile_image());
        }

        object.putAll(map);
        return object;
    }

    public EmployeeBuilder parseEmployeeJSON(JSONObject obj){
        this.employee = new Employee();
        String data = obj.get("data").toString();
        obj = JsonParser.convertJsonToObject(data);
        if(obj.keySet().contains("id")) {
            this.employee.setId(Integer.parseInt(obj.get("id").toString()));
        }
        if(obj.keySet().contains("age")) {
            this.employee.setAge(Integer.parseInt(obj.get("age").toString()));
        }
        if(obj.keySet().contains("salary")) {
            this.employee.setEmployee_salary(obj.get("salary").toString());
        }
        if(obj.keySet().contains("image")) {
            this.employee.setProfile_image(obj.get("image").toString());
        }
        if(obj.keySet().contains("name")) {
            this.employee.setEmployee_name(obj.get("name").toString());
        }
        return this;
    }

    public EmployeeBuilder setEmployee(Employee employee){
        this.employee = employee;
        return this;
    }

    public EmployeeBuilder withId(int id) {
        this.employee.setId(id);
        return this;
    }
}

package suites;

import lib.EmployeeBuilder;
import models.Config;
import models.Employee;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.EmployeeApi;
import utilities.ConfigLoader;
import utilities.ContextLoader;
import lib.HttpClient;
import utilities.WrapperRestAssured;

import java.io.File;

public class Dummy {

    private  EmployeeApi employeeApi;

    @BeforeTest
    public void setup(){
        File file = new File("resources/config.json");
        Config config = ConfigLoader.parseConfig(file).getConfig();
        ContextLoader.parseConfig(new File("resources/context.json"));
        HttpClient client = new WrapperRestAssured(config);
        this.employeeApi = new EmployeeApi(client);
    }

    @Test
    public void verifyEmployeeCreation(){
        Employee employee = new EmployeeBuilder().withName("x").withAge(10).getEmployee();
        Employee employee1 = this.employeeApi.createEmployee(employee);
        assert employee.equals(employee1);
    }

    @AfterTest
    public void teardown(){

    }
}

package models;

import java.util.List;

public class Config {
    private String hosts;
    private int port;
    private String connectionType;
    private List<Header> headers;

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Header> headers) {
        this.headers = headers;
    }

    public void removeHeader(String name){
        for(Header header : this.headers){
            if(header.getName().equals(name)) {
                this.headers.remove(header);
                return;
            }
        }
    }

    public void addHeader(String name, String value){
        this.headers.add(new Header().setName(name).setValue(value));
    }

    public void addHeader(Header header){
        this.headers.add(header);
    }


}

package models;

public class Header {

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public Header setName(String name) {
        this.name = name;
        return this;
    }

    public String getValue() {
        return value;
    }

    public Header setValue(String value) {
        this.value = value;
        return this;
    }
}

package models;

public class Employee {
    private Integer id;
    private String employee_name;
    private String employee_salary;
    private Integer age;
    private String profile_image;

    public void setId(int id){
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_salary() {
        return employee_salary;
    }

    public void setEmployee_salary(String employee_salary) {
        this.employee_salary = employee_salary;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(int aga) {
        this.age = age;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }
}

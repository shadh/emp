package pages;

import lib.EmployeeBuilder;
import lib.HttpClient;
import models.Employee;
import models.HttpResponse;
import org.json.simple.JSONObject;
import utilities.ContextLoader;
import utilities.JsonParser;

import java.util.List;

public class EmployeeApi {

    private String employeeContext;
    private HttpClient client;

    public EmployeeApi(HttpClient client){
        this.employeeContext = ContextLoader.getContext("employee");
        this.client = client;
    }

    public Employee getEmployee(Employee employee){

        return null;
    }

    public List<Employee> getAllEmployees(){
        return null;
    }

    public boolean deleteEmployeeById(Employee employee){
        return true;
    }

    public boolean updateEmployeeById(Employee employee){
        return true;
    }

    public Employee createEmployee(Employee employee){
        HttpResponse response = this.client.requestBody(new EmployeeBuilder().setEmployee(employee).getEmployeeJSON()).post(this.employeeContext);
        Employee employee1 = new EmployeeBuilder().parseEmployeeJSON(JsonParser.convertJsonToObject(response.getMessage())).getEmployee();
        return employee1;
    }

}

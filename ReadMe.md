About the project
-------------------------------
This project is designed to test rest api or simple http api. It followed conventional approach of organisation folder.

Pre-requisites
--------------------------------


General Instruction
------------------------------
It has 6 folders with convention:

1. lib : It have library which directly used by pages(actual requirement of test). Characteristics of library is that it
is independent of utility underneath working.
2. models : It is POJO(plain old java object) which represents any entities of concern.
3. suites : It holds test suites which underneath using test library like TestNG.
4. utilities : It is where availed library has been integrated and build api to consume by lib.
    Note: lib loads utilities, at the same time it is independent of any particular utility.
5.pages : It is where requirement based functionality has be provided which later been used in suites.
6. resources : It holds various configuration, intended to avoid hard coding in the program.


 For Developer
 -------------------------------
 
 
 
 For User
 --------------------------------
 
 
 Deployment instruction to CI
 ---------------------------------